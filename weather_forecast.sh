# weather_forecast.sh
# a quick-and-dirty shell script for checking the weather.

# notes:
# - an optional location can be specified as a U.S. zip code or a city
# - units supported include fahrenheit and celsius, with the latter as default.

URL="wttr.in/"
LOCATION="$1"
UNIT_ARG=${!#}

# default to celsius
if [ "$UNIT_ARG" = "F" ]
then
    UNIT="?u"
else
    UNIT="?m"
fi

# if there is a unit but no location, we need to
# replace the location with the empty string
if [ "$LOCATION" = "F" ] || [ "$LOCATION" = "C" ]
then
    LOCATION=""
fi

curl "$URL$LOCATION$UNIT"
